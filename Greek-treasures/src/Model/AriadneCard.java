/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author John
 */
public class AriadneCard extends SpecialCard {

    AriadneCard(int path) {
        super(path);
        this.value = 0;
        this.step = 2;
        this.source_path = "resources/images/cards/" + path + "Min.jpg";
        switch (path) {
            case 0:
                this.source_path = "resources/images/cards/KnossosAri.jpg";
                break;
            case 1:
                this.source_path = "resources/images/cards/MaliaAri.jpg";
                break;
            case 2:
                this.source_path = "resources/images/cards/PhaistosAri.jpg";
                break;
            case 3:
                this.source_path = "resources/images/cards/ZakrosAri.jpg";
                break;
        }

    }

    public String toString() {
        return "Karta Ariadne " + this.path;
    }

}
