/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author John
 */
public class FindingsPosition extends Position {

    public FindingsPosition(String path,int position) {
        this.path = path;
        this.boardPosition=position;
        //  this.checkPoint=false;
        
        switch (this.path) {
            case "KnossosFind":
                source_path = "resources/images/paths/knossos2.jpg";
                break;
            case "MaliaFind":
                source_path = "resources/images/paths/malia2.jpg";
                break;
            case "PhaistosFind":
                source_path = "resources/images/paths/phaistos2.jpg";
                break;
            case "ZakrosFind":
                source_path = "resources/images/paths/zakros2.jpg";
                break;
            case "KnossosPalace":
                source_path = "resources/images/paths/knossosPalace.jpg";
                break;
            case "MaliaPalace":
                source_path = "resources/images/paths/maliaPalace.jpg";
                break;
            case "PhaistosPalace":
                source_path = "resources/images/paths/phaistosPalace.jpg";
                break;
            case "ZakrosPalace":
                source_path = "resources/images/paths/zakrosPalace.jpg";
                break;

        }
    }

}
