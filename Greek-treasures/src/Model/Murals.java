/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author John
 */
public class Murals extends Findings {

    /**
     * Create a new mural depending on the number and set it to a path.
     *
     * @param path the path to be placed
     * @param number number of mural
     * @throws FileNotFoundException
     * @throws IOException
     */

    public Murals(int path, int number) throws FileNotFoundException, IOException {
        super(path);
        String b[][] = new String[16][3];
        String splitBy = ",";
        BufferedReader br = new BufferedReader(new FileReader("resources/csvFiles/csv_greeklish.csv"));
        String line = br.readLine();
        int i = 0;
        while ((line = br.readLine()) != null) {
            b[i++] = line.split(splitBy);

        }
        br.close();
        switch (number) {
            case 1:
                this.source_path = "resources/images/findings/fresco" + Integer.toString(number) + "_20.jpg";
                // this.message = b[5 + number][1];
                this.message = b[5][1];
                this.description = b[5][2];
                this.points = 20;

                break;
            case 2:
                this.source_path = "resources/images/findings/fresco" + Integer.toString(number) + "_20.jpg";
                this.points = 20;
                this.message = b[6][1];
                this.description = b[6][2];

                break;
            case 3:
                this.source_path = "resources/images/findings/fresco" + Integer.toString(number) + "_15.jpg";
                this.points = 15;
                this.message = b[7][1];
                this.description = b[7][2];

                break;
            case 4:
                this.source_path = "resources/images/findings/fresco" + Integer.toString(number) + "_20.jpg";
                this.points = 20;
                this.message = b[8][1];
                this.description = b[8][2];
                break;

            case 5:
                this.source_path = "resources/images/findings/fresco" + Integer.toString(number) + "_15.jpg";
                this.points = 15;
                this.message = b[9][1];
                this.description = b[9][2];

                break;
            case 6:
                this.source_path = "resources/images/findings/fresco" + Integer.toString(number) + "_15.jpg";
                this.points = 15;
                this.message = b[10][1];
                this.description = b[10][2];

                break;
        }

    }

}
