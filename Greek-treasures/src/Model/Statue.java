/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Statue extends Findings {

    private boolean discovered;

    public Statue(int path) throws FileNotFoundException, IOException {
        super(path);
         String b[][] = new String[16][3];
        String splitBy = ",";
        BufferedReader br = new BufferedReader(new FileReader("resources/csvFiles/csv_greeklish.csv"));
        String line = br.readLine();
        int i = 0;
        while ((line = br.readLine()) != null) {
            b[i++] = line.split(splitBy);

        }
        br.close();
        this.description=b[4][2];
        this.message=b[4][1];
        this.source_path = "resources/images/findings/snakes.jpg";
        this.points = 0;
        

    }

    public boolean isObtainable() {
        if (this.discovered) {
            return false;
        } else {
            return true;
        }
    }

    public void discovered() {
        this.discovered = true;
    }
}
