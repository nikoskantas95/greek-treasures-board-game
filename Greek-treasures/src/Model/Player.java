/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import View.*;

/**
 *
 * @author John
 */
public class Player {

    private String name;
    private int points;
    private int cardsInHand;
    private Pawn[] pawns;
    private Card[] deck;
    private int paths[];
    private boolean turn, discard;
    private int path1_c, path2_c, path3_c, path4_c;
    private ArrayList<Murals> murals;
    private ArrayList<RareFinding> rare_findings;
    private ArrayList<Statue> statues;

    /**
     * Constructs a new player
     *
     * @param name the name of the player
     */
    public Player(String name) {

        this.name = name;
        this.points = 0;
        this.cardsInHand = 0;
        this.path1_c = 0;
        this.path2_c = 0;
        this.path3_c = 0;
        this.path4_c = 0;
        this.discard = false;
        murals = new ArrayList<Murals>();
        rare_findings = new ArrayList<RareFinding>();
        statues = new ArrayList<Statue>();
        pawns = new Pawn[4];
        deck = new Card[8];
        for (int i = 0; i < 3; i++) {
            pawns[i] = new Arc(this);
        }
        pawns[3] = new Theseus(this);

        paths = new int[4];
        for (int i = 0; i < 4; i++) {
            paths[i] = -1;
        }

    }

    /**
     * Returns the player's name
     *
     * @return
     */

    public String getName() {
        return this.name;
    }

    /**
     * Returns the player's points.
     *
     * @return
     */

    public int getPoints() {
        return this.points;
    }

    /**
     * Sets the player's points.
     *
     * @param val
     */

    public void setPoints(int val) {
        this.points = val;
    }

    /**
     * Add a value to the player's points
     *
     * @param val
     */
    public void addPoints(int val) {
        this.points = this.points + val;
    }

    /**
     * returns the index pawn of the player.
     *
     * @param index
     * @return
     */

    public Pawn getPawn(int index) {
        return this.pawns[index];
    }

    /**
     * Adds a new card to the player's hand.
     *
     * @param d the deck to draw
     * @param index number of card
     */
    public void addCard(Deck d, int index) {

        deck[index] = d.drawCard();

    }

    /**
     * Returns the index card of the player's hand/
     *
     * @param index
     * @return
     */
    public Card getCard(int index) {
        return this.deck[index];
    }

    /**
     * Setting the paths array such as the index path in the corresponding board
     * to have pawn.
     *
     * @param index
     */

    public void setPath(int index) {
        paths[index] = 1;
    }

    /**
     * Returns the index cell of paths array
     *
     * @param index
     * @return
     */

    public int getPath(int index) {
        return paths[index];
    }

    /**
     * Sets the player's turn.
     *
     * @param flag
     */
    public void setTurn(boolean flag) {
        this.turn = flag;

    }

    /**
     * True if is the player's turn else false.
     *
     * @return
     */
    public boolean getTurn() {
        return this.turn;
    }

    /**
     * Sets the path's current counter(equals to the last played card value)
     *
     * @param val
     */
    public void setCounter1(int val) {
        if (val > this.path1_c) {
            this.path1_c = val;
        }
    }

    /**
     * Returns the paths current counter(last path card value)
     *
     * @return
     */

    public int getCounter1() {
        return path1_c;
    }

    /**
     * Sets the path's current counter(equals to the last played card value)
     *
     * @param val
     */
    public void setCounter2(int val) {
        if (val > this.path2_c) {
            this.path2_c = val;
        }
    }

    /**
     * Returns the paths current counter(last path card value)
     *
     * @return
     */
    public int getCounter2() {
        return path2_c;
    }

    /**
     * Sets the path's current counter(equals to the last played card value)
     *
     * @param val
     */

    public void setCounter3(int val) {
        if (val > this.path3_c) {
            this.path3_c = val;
        }
    }

    /**
     * Returns the paths current counter(last path card value)
     *
     * @return
     */
    public int getCounter3() {
        return path3_c;
    }

    /**
     * Sets the path's current counter(equals to the last played card value)
     *
     * @param val
     */

    public void setCounter4(int val) {
        if (val > this.path4_c) {
            this.path4_c = val;
        }
    }

    /**
     * Returns the paths current counter(last path card value)
     *
     * @return
     */
    public int getCounter4() {
        return path4_c;
    }

    /**
     * Checks if the card to be played is valid.
     *
     * @param c
     * @return
     */

    public boolean validCard(Card c) {
        boolean return_val = false;
        if (c instanceof SpecialCard) {
            return true;
        }
        switch (c.getPath()) {
            case 0:
                if (c.getValue() >= this.path1_c) {
                    return_val = true;
                } else {
                    return_val = false;
                }
                break;
            case 1:
                if (c.getValue() >= this.path2_c) {
                    return_val = true;
                } else {
                    return_val = false;
                }
                break;
            case 2:
                if (c.getValue() >= this.path3_c) {
                    return_val = true;
                } else {
                    return_val = false;
                }
                break;
            case 3:
                if (c.getValue() >= this.path4_c) {
                    return_val = true;
                } else {
                    return_val = false;
                }
                break;

        }
        return return_val;
    }

    /**
     * Adds a mural to the player's collection.
     *
     * @param mural
     */
    public void addMural(Murals mural) {
        this.murals.add(mural);
    }

    /**
     * Returns the player's mural collection.
     *
     * @return
     */

    public ArrayList<Murals> getMurals() {
        return this.murals;
    }

    /**
     * Adds a rare to the player's collection.
     *
     * @param rare
     */
    public void addRare(RareFinding rare) {
        this.rare_findings.add(rare);
    }

    /**
     * Returns the player's rare collection.
     *
     * @return
     */

    public ArrayList<RareFinding> getRare() {
        return this.rare_findings;
    }

    /**
     * Adds a statue to the player's collection.
     *
     * @param statue
     */
    public void addStatue(Statue statue) {
        this.statues.add(statue);
    }

    /**
     * Returns the player's mural collection.
     *
     * @return
     */

    public ArrayList<Statue> getStatues() {
        return this.statues;
    }

    /**
     * Flag that controls if the player wants to discard a card or not.
     *
     * @param val
     */
    public void setDiscard(boolean val) {
        this.discard = val;
    }

    /**
     * Return the setDiscard flag.
     *
     * @return
     */
    public boolean getDiscard() {
        return this.discard;
    }
}
