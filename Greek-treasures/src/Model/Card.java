/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author John
 */
public abstract class Card {

    //private  String name;
    protected int path;
    protected String source_path;
    protected int step;
    protected int value;

    /**
     * Constructs a new Card
     *
     * @param path
     * @param value
     */

    Card(int path, int value) {
        this.path = path;
        this.step = 1;
        this.value = value;

    }

    /**
     * Returns the card's path.
     *
     * @return
     */

    public int getPath() {
        return this.path;
    }

    /**
     * Returns the card's step.
     *
     * @return
     */

    public int getStep() {
        return step;
    }

    /**
     * Returns the card's source path.
     *
     * @return
     */
    public String getSourcePath() {
        return this.source_path;
    }

    /**
     * Returns the card's value.
     *
     * @return
     */
    public int getValue() {
        return this.value;
    }
}
