/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author John
 */
public abstract class Pawn {

    protected String name;
    protected String source_path;
    protected int path;
    protected Player owner;
    protected boolean isProtected;
    private boolean canPlace;
    int position;
    int points;

    /**
     * Constructs a new Pawn.
     */
    public Pawn() {
        this.canPlace = true;
        this.position = -1;
        this.points = 0;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Player getOwner() {
        return this.owner;
    }

    /**
     * Check if the pawn is protected from a Minotaur attack.
     *
     * @return true or false
     */

    public boolean isProtected() {
        return this.isProtected;
    }

    /**
     * Sets the pawn to be protected from Minotaur attack.
     */
    public void setProtection() {
        this.isProtected = true;
    }

    /**
     * Returns the pawn's position
     *
     * @return
     */

    public int getPosition() {
        return this.position;
    }

    /**
     * Sets the pawn's position on the board.
     *
     * @param val the played card step.
     */

    public void setPosition(int val) {
        if (this.position + val <= 8) {
            this.position = this.position + val;
        } else if (this.position +val >= 8) {
            this.position = 8;
        }
        if (this.position == -1) {
            this.position = 0;
        }
    }

    /**
     * Returns the pawn's sourc path.
     *
     * @return
     */
    public String getSourcePath() {
        return this.source_path;
    }

    /**
     * Sets the pawn's source path
     *
     * @param newpath
     */
    public void setSourcePath(String newpath) {
        this.source_path = newpath;
    }

    /**
     * Returns true if the pawn can be placed,else false
     *
     * @return
     */
    public boolean canPlaced() {
        return this.canPlace;
    }

    /**
     * Sets the pawn unplaceable.
     */
    public void unplaceable() {
        this.canPlace = false;
    }

    /**
     * Sets the pawn's path
     *
     * @param path
     */
    public void setPath(int path) {
        this.path = path;
    }

    /**
     * Returns the pawn's path
     *
     * @return
     */
    public int getPath() {
        return this.path;
    }

    /**
     * Returns the pawn's points.
     *
     * @return
     */
    public int getPawnPoints() {
        return this.points;
    }

    /**
     * Sets the pawn's points
     *
     * @param val
     */
    public void setPawnPoints(int val) {
        this.points = val;
    }

}
