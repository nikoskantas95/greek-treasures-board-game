/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author John
 */
public abstract class Findings {

    protected int points;
    // protected Player owner;
    protected String source_path;
    protected String message;
    protected String description;
    protected int position, path;
    protected boolean found;

    public Findings(int path) {
        this.path = path;
        this.found = false;
    }

    /**
     * Returns a Findings points.
     *
     * @return
     */

    public int getPoints() {
        return this.points;
    }

    /**
     * Sets a Finding to a position
     *
     * @param val the position
     */

    public void setPosition(int val) {
        this.position = val;
    }

    /**
     * Returns the Findings position
     *
     * @return
     */

    public int getPosition() {
        return this.position;
    }

    /**
     * Returns the Findings source path string.
     *
     * @return
     */

    public String getSourcePath() {
        return this.source_path;
    }

    /**
     * Returns true if Finding is already discovered false if not.
     *
     * @return
     */

    public boolean isDiscovered() {
        return this.found;
    }

    /**
     * Sets the finding as discovered.
     */

    public void discovered() {
        this.found = true;
    }

    /**
     * Return the Finding's message.
     *
     * @return
     */

    public String getMessage() {
        return this.message;
    }

    /**
     * Return the Findings Description.
     *
     * @return
     */

    public String getDescription() {
        return this.description;
    }

}
