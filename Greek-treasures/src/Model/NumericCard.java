/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author John
 */
public class NumericCard extends Card {

    NumericCard(int path, int value) {
        super(path, value);
        switch (path) {
            case 0:
                this.source_path = "resources/images/cards/Knossos" + value + ".jpg";
                break;
            case 1:
                this.source_path = "resources/images/cards/Malia" + value + ".jpg";
                break;
            case 2:
                this.source_path = "resources/images/cards/Phaistos" + value + ".jpg";
                break;
            case 3:
                this.source_path = "resources/images/cards/Zakros" + value + ".jpg";
                break;
        }

    }

}
