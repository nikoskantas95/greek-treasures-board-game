/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class RareFinding extends Findings {

    private boolean discovered;

    public RareFinding(int path) throws FileNotFoundException, IOException {
        super(path);
        String b[][] = new String[16][3];
        String splitBy = ",";
        BufferedReader br = new BufferedReader(new FileReader("resources/csvFiles/csv_greeklish.csv"));
        String line = br.readLine();
        int i = 0;
        while ((line = br.readLine()) != null) {
            b[i++] = line.split(splitBy);

        }
        br.close();
        this.discovered = false;
        switch (path) {

            case 0:
                this.source_path = "resources/images/findings/ring.jpg";
                this.points = 25;
                this.message = b[3][1];
                this.description = b[3][2];
                break;
            case 1:
                this.source_path = "resources/images/findings/kosmima.jpg";
                this.points = 25;
                this.message = b[1][1];
                this.description = b[1][2];
                break;
            case 2:
                this.source_path = "resources/images/findings/diskos.jpg";
                this.points = 35;
                this.message = b[0][1];
                this.description = b[0][2];
                break;
            case 3:
                this.source_path = "resources/images/findings/ruto.jpg";
                this.points = 25;
                this.message = b[2][1];
                this.description = b[2][2];
                break;
        }
    }

    public boolean isObtainable() {
        if (this.discovered) {
            return false;
        } else {
            return true;
        }
    }

    public void discovered() {
        this.discovered = true;
    }
}
