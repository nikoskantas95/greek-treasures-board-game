/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author John
 */
public abstract class Position {

    protected int boardPosition;
    protected int points;
    String path;
    String source_path;

    /**
     * Sets the position on the board.
     *
     * @param pos
     */
    public void setPosition(int pos) {
        this.boardPosition = pos;
    }

    /**
     * Returns the position's position on the board.
     *
     * @return
     */

    public int getPosition() {
        return this.boardPosition;
    }

    /**
     * Set's the position's worth of points.
     *
     * @param val
     */
    public void setPositionPoints(int val) {
        this.points = val;
    }

    /**
     * Return the position's worth of points.
     *
     * @return
     */
    public int getPositionPoints() {
        return this.points;
    }

    public String getSourcePath() {
        return this.source_path;
    }

}
