/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Random;

/**
 *
 * @author John
 */
public class Deck {

    private Card collection[];
    protected String source_path;
    private Card a, noCard;
    protected int available;
    protected static int nextCard;
    protected boolean draw;

    /**
     * Constructs a new Deck.
     */
    public Deck() {
        this.source_path = "resources/images/cards/backCard.jpg";
        this.collection = new Card[100];
        this.available = 100;
        this.nextCard = 0;
        this.draw = true;

        int power = 1;
        for (int i = 0; i < 40; i = i + 4) { //adding the numeric cards 1-40

            collection[i] = new NumericCard(0, power);
            collection[i + 1] = new NumericCard(1, power);
            collection[i + 2] = new NumericCard(2, power);
            collection[i + 3] = new NumericCard(3, power);
            power++;
        }
        power = 1;

        for (int i = 0; i < 40; i = i + 4) { //adding the numeric cards 40-80
            collection[i + 40] = new NumericCard(0, power);
            collection[i + 41] = new NumericCard(1, power);
            collection[i + 42] = new NumericCard(2, power);
            collection[i + 43] = new NumericCard(3, power);
            power++;
        }

        for (int i = 0; i < 12; i = i + 4) {
            collection[i + 80] = new AriadneCard(0);
            collection[i + 81] = new AriadneCard(1);
            collection[i + 82] = new AriadneCard(2);
            collection[i + 83] = new AriadneCard(3);

        }
        for (int i = 0; i < 8; i = i + 4) {
            collection[i + 92] = new MinotaurCard(0);
            collection[i + 93] = new MinotaurCard(1);
            collection[i + 94] = new MinotaurCard(2);
            collection[i + 95] = new MinotaurCard(3);

        }
        noCard = new NumericCard(-1, -1);
        shuffleDeck();

    }

    /**
     * Shuffles the deck.
     */

    public void shuffleDeck() {
        Random r = new Random();

        for (int i = 100 - 1; i > 0; i--) {
            int aux = r.nextInt(100);

            // Simple swap
            a = collection[aux];

            collection[aux] = collection[i];

            collection[i] = a;
        }
    }

    /**
     * Draws the next card and returns it.
     *
     * @return a new card,or null if there are no cards available
     */

    public Card drawCard() {

        if (this.available > 0) {
            nextCard++;
            available = available - 1;

            return collection[nextCard - 1];
        } else {
            this.draw = false;
            return noCard;
        }

    }

    /**
     * Returns the number of available cards.
     *
     * @return available cards
     */
    public int getAvailableCards() {
        return this.available;
    }

    /**
     * Returns the source string.
     *
     * @return
     */

    public String getPath() {
        return this.source_path;
    }

    /**
     * Returns a boolean value depending on if the deck is drawable.
     *
     * @return
     */
    public boolean canDraw() {
        return this.draw;
    }

}
