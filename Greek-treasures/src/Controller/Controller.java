/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

/**
 *
 * @author John
 */
import View.WinnerWindow;
import Model.*;
import View.*;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author John
 */
public class Controller {

    Player p1, p2;

    Deck deck;
    Board board;
    static int draw = 7;
    private int checkpoints;

    /**
     * Constructor. Constructs a Controller who is responsible for updating the
     * View through the Model
     *
     * @throws IOException
     */
    Controller() throws IOException {
        initialize();
        board = new Board(this);

    }

    /**
     * Initialization of the game.Player1 starts first.
     */
    public void initialize() {
        this.p1 = new Player("Player 1");
        for (int i = 0; i < 3; i++) {
            p1.getPawn(i).setSourcePath("resources/images/pionia/arch1.jpg");
        }
        p1.getPawn(3).setSourcePath("resources/images/pionia/theseus1.jpg");
        this.p2 = new Player("Player 2");
        for (int i = 0; i < 3; i++) {
            p2.getPawn(i).setSourcePath("resources/images/pionia/arch2.jpg");
        }
        p2.getPawn(3).setSourcePath("resources/images/pionia/theseus2.jpg");
        p1.setTurn(true);
        p2.setTurn(false);
        this.deck = new Deck();
        for (int i = 0; i < 8; i++) {
            p1.addCard(deck, i);
            p2.addCard(deck, i);
        }
        this.checkpoints = 0;
    }

    /**
     * Function responsible for moving a player's pawn and calling the View
     * functions in order to update the Board.
     *
     * @param index number of card in hand
     * @param pawn players pawn
     * @param card the corresponding JButton
     */
    public void movePawn(int index, Pawn pawn, JButton card) {
        //board.printPoints();
        /*   Move player 1  */
        if (p1.getTurn() && p1.validCard(p1.getCard(index))) {
            int path = pawn.getPath(), pos = 0;
            Card playing_card = p1.getCard(index);
            Findings aux_find;

            if ((playing_card instanceof NumericCard || playing_card instanceof AriadneCard)) {
                p1.addCard(deck, index);
                Card new_card = p1.getCard(index);
                int new_position = playing_card.getStep();

                pawn.setPosition(new_position);
                pos = pawn.getPosition();
                if (pos == 8) {
                    try {
                        FinishWindow window = new FinishWindow(path);
                    } catch (IOException ex) {
                        Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                pawn.setPawnPoints(board.getBoardPosition(path, pos).getPositionPoints());
                setPathCounters(p1, playing_card);
                //status(p1);
                aux_find = board.getFindingPosition(path, pos);
                if (playing_card instanceof AriadneCard) {
                    if (pos % 2 == 0 || pos == 8) {
                        addFindings(p1, path, pos - 1);
                    } else {
                        addFindings(p1, path, pos);
                    }
                } else {
                    addFindings(p1, path, pos);
                }
                if (pos == 6) {
                    checkpoints++;
                }
                updatePoints(p1);
                board.updateInfo(p1);
                board.updateBoard(new_card, path, pos, card);

                p1.setTurn(false);
                p2.setTurn(true);

            } else if (playing_card instanceof MinotaurCard) {

                p1.addCard(deck, index);
                Card new_card = p1.getCard(index);

                pawn.setPawnPoints(board.getBoardPosition(path, pos).getPositionPoints());
                if (pawn.isProtected() == false) {
                    int new_position = playing_card.getStep();
                    pawn.setPosition(new_position);
                    pos = pawn.getPosition();

                    p1.setTurn(false);
                    p2.setTurn(true);
                    updatePoints(p2);
                    board.updateBoard(new_card, path, pos, card);
                    board.updateInfo(p2);

                }

            }

        } else if (p2.getTurn() && p2.validCard(p2.getCard(index))) {
            int path = pawn.getPath(), pos = 0;
            Card playing_card = p2.getCard(index);

            if (playing_card instanceof NumericCard || playing_card instanceof AriadneCard) {
                p2.addCard(deck, index);
                Card new_card = p2.getCard(index);
                int new_position = playing_card.getStep();
                pawn.setPosition(new_position);

                pos = pawn.getPosition();
                if (pos == 8) {
                    try {
                        FinishWindow window = new FinishWindow(path);
                    } catch (IOException ex) {
                        Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                pawn.setPawnPoints(board.getBoardPosition(path, pos).getPositionPoints());
                if (pawn.getPosition() >= 7) {
                    pawn.setProtection();
                }

                setPathCounters(p2, playing_card);
                // status(p2);
                if (playing_card instanceof AriadneCard) {
                    if (pos % 2 == 0 || pos == 8) {
                        addFindings(p2, path, pos - 1);
                    } else {
                        addFindings(p2, path, pos);
                    }
                } else {
                    addFindings(p2, path, pos);
                }
                if (pos == 6) {
                    checkpoints++;
                }
                updatePoints(p2);
                board.updateInfo(p2);
                board.updateBoard(new_card, path, pos, card);

                p2.setTurn(false);
                p1.setTurn(true);

            } else if (playing_card instanceof MinotaurCard) {
                //  pos = pawn.getPosition();
                p2.addCard(deck, index);
                Card new_card = p2.getCard(index);
                if (pawn.isProtected() == false) {
                    int new_position = playing_card.getStep();
                    pawn.setPosition(new_position);
                    pos = pawn.getPosition();
                    pawn.setPawnPoints(board.getBoardPosition(path, pos).getPositionPoints());
                    p2.setTurn(false);
                    p1.setTurn(true);
                    updatePoints(p2);
                    board.updateBoard(new_card, path, pos, card);
                    board.updateInfo(p2);

                }

            }

        }
        checkWinner();

    }

    /**
     * Works as the movePawn function,with the difference that works only for
     * the first move of a new path
     *
     * @param index card in hand
     * @param button corresponding button
     */
    public void moveNew(int index, JButton button) {
        PawnsCollection window;
        if (p1.getTurn()) {
            window = new PawnsCollection(p1, this, index, button);
        } else {
            window = new PawnsCollection(p2, this, index, button);
        }

    }

    /**
     * Checks if a position is instance of FindingsPosition. If it is,check if
     * the finding is obtainable and add it to player's collection.
     *
     * @param player the current player
     * @param i path
     * @param j position
     */
    public void addFindings(Player player, int i, int j) {
        if (this.board.getBoardPosition(i, j) instanceof FindingsPosition) {
            if (this.board.getFindingPosition(i, j) instanceof RareFinding) {
                RareFinding finding = (RareFinding) this.board.getFindingPosition(i, j);
                if (finding.isObtainable()) {
                    player.addRare(finding);
                    finding.discovered();
                    board.updateRare(player, i);
                    RareWindow window = new RareWindow(finding);

                }

                // 
            } else if (this.board.getFindingPosition(i, j) instanceof Statue) {
                Statue finding = (Statue) this.board.getFindingPosition(i, j);
                if (finding.isObtainable()) {
                    player.addStatue(finding);
                    finding.discovered();
                    StatueWindow window = new StatueWindow(finding);

                }

            } else if (this.board.getFindingPosition(i, j) instanceof Murals) {

                Murals mural = (Murals) this.board.getFindingPosition(i, j);
                MuralWindow window = new MuralWindow(player, mural);
                player.addMural(mural);
            }

        }
    }

    /**
     * Removes a card from player's hand and replaces it with a new one.
     *
     * @param index card in hand
     * @param card corresponding button.
     */
    public void discardCard(int index, JButton card) {
        if (p1.getTurn()) {

            p1.addCard(deck, index);
            Card new_card = p1.getCard(index);
            board.updateHand(new_card, card);
            p1.setTurn(false);
            p1.setDiscard(false);
            p2.setTurn(true);
        } else if (p2.getTurn()) {
            p2.addCard(deck, index);
            Card new_card = p2.getCard(index);
            board.updateHand(new_card, card);
            p2.setTurn(false);
            p2.setDiscard(false);
            p1.setTurn(true);
        }

    }

    /* public static void updateCheckPoints(){
        for(int i=0;i<4;i++){
            for(int j=0;j<9;j++){
                if(p1.getPawn(i).getBoar)
            }
        }
    }*/
    /**
     * Returns a position's worth of points/
     *
     * @param pos current position
     * @return points
     */
    public int findingsPoints(Findings pos) {
        if (pos != null) {
            return pos.getPoints();
        } else {
            return 0;
        }

    }

    /**
     * Returns the deck
     *
     * @return deck
     */
    public Deck getDeck() {
        return this.deck;
    }

    /**
     * Returns player1
     *
     * @return a player
     */
    public Player getPlayer1() {
        return this.p1;
    }

    /**
     * Returns Player 2
     *
     * @return a player
     */
    public Player getPlayer2() {
        return this.p2;
    }

    /**
     * Given a path,returns the index of the corresponding pawn of that path
     * else -1 if there is no pawn in the path
     *
     * @param path the path
     * @param player current player
     * @return index of a pawn if there is one
     */
    public int comparingPaths(int path, Player player) {
        int return_val = -1;
        for (int i = 0; i < 4; i++) {
            if (path == player.getPawn(i).getPath()) {
                return_val = i;
                break;
            }
        }
        return return_val;
    }

    /**
     * Returns the Controller's board
     *
     * @return the board
     */
    public Board getBoard() {
        return this.board;
    }

    /**
     * Calculates a player's points from his pawns
     *
     * @param player the current player
     * @return points from pawns
     */
    public static int pawnPoints(Player player) {
        int pts = 0;
        for (int i = 0; i < 4; i++) {

            pts = pts + player.getPawn(i).getPawnPoints();

        }
        return pts;
    }

    /**
     * Calculates and updates a players total points
     *
     * @param player
     */
    public static void updatePoints(Player player) {
        //From pawns
        int pawn_pts = 0;
        int rare_pts = 0;
        int statue_pts = 0;
        int mural_pts = 0;
        int aux;
        for (int i = 0; i < 4; i++) {
            if (player.getPawn(i) instanceof Arc) {
                pawn_pts = pawn_pts + player.getPawn(i).getPawnPoints();
            } else if (player.getPawn(i) instanceof Theseus) {
                pawn_pts = pawn_pts + player.getPawn(i).getPawnPoints() * 2;
            }
        }
        // From rares
        player.setPoints(pawn_pts);
        for (int i = 0; i < player.getRare().size(); i++) {
            rare_pts = player.getRare().get(i).getPoints();
            player.addPoints(rare_pts);

        }

        //From statues
        aux = player.getStatues().size();
        switch (aux) {
            case 0:
                statue_pts = 0;
                break;
            case 1:
                statue_pts = -20;
                break;
            case 2:
                statue_pts = -15;
                break;
            case 3:
                statue_pts = 5;
                break;
            case 4:
                statue_pts = 15;
                break;
            case 5:
                statue_pts = 30;
                break;
            case 6:
                statue_pts = 45;
                break;
            case 7:
                statue_pts = 55;
                break;
            case 8:
                statue_pts = 70;
                break;
            case 9:
                statue_pts = 90;
                break;
            case 10:
                statue_pts = 100;
                break;
        }
        player.addPoints(statue_pts);

        // from murals
        for (int i = 0; i < player.getMurals().size(); i++) {
            mural_pts = mural_pts + player.getMurals().get(i).getPoints();
        }
        player.addPoints(mural_pts);

    }

    /**
     * Auxilery function for debug.
     *
     * @param p
     */
    public void status(Player p) {

        for (int i = 0; i < 4; i++) {

            System.out.println("[" + i + "]:" + p.getPawn(i).getName() + "----->" + p.getPawn(i).getPath() + "------->Position:" + p.getPawn(i).getPosition());

        }
        //System.out.println("\n");

        for (int i = 0; i < 4; i++) {

            System.out.println("[" + p.getPath(i) + "]");
        }

        System.out.println("\n");
    }

    /**
     * Return a string with the current player's path counters
     *
     * @param pthe current player
     * @return a string
     */
    public String getPathCounter(Player p) {
        return ("<br>" + p.getCounter1() + " " + p.getCounter2() + " " + p.getCounter3() + " " + p.getCounter4() + " ");
    }

    /**
     * Changes the counters(card value limits) of a path for a player.
     *
     * @param p the current player.
     * @param c the card being played.
     */
    public static void setPathCounters(Player p, Card c) {
        int path = c.getPath();
        int counter = c.getValue();

        switch (path) {
            case 0:
                p.setCounter1(counter);
                break;
            case 1:
                p.setCounter2(counter);
                break;
            case 2:
                p.setCounter3(counter);
                break;
            case 3:
                p.setCounter4(counter);
                break;
        }
    }

    /**
     * Check if the condition for having a winner are met.
     */
    public void checkWinner() {
        if (checkpoints == 3 || deck.getAvailableCards() == 0) {
            Player winner = getWinner();
            Player loser = null;
            if (winner == p1) {
                loser = p2;
            } else if (winner == p2) {
                loser = p1;
            }

            WinnerWindow window = new WinnerWindow(winner);
        }
    }

    /**
     * Returns the current checkpoints
     *
     * @return
     */
    public int getCheckPoints() {
        return this.checkpoints;
    }

    /**
     * Return the winner of the game if there is someone.
     *
     * @return
     */
    public Player getWinner() {

        if (p1.getPoints() > p2.getPoints()) {
            return p1;
        } else if (p1.getPoints() < p2.getPoints()) {
            return p2;
        } else {
            /*    a   */
            if (p1.getRare().size() > p2.getRare().size()) {
                return p1;
            } else if (p1.getRare().size() < p2.getRare().size()) {
                return p2;
            } else {
                /*    b   */
                if (p1.getMurals().size() > p2.getMurals().size()) {
                    return p1;
                } else if (p1.getMurals().size() < p2.getMurals().size()) {
                    return p2;
                } else {
                    /*    c   */
                    if (p1.getStatues().size() > p2.getStatues().size()) {
                        return p1;
                    } else if (p1.getStatues().size() < p2.getStatues().size()) {
                        return p2;
                    }
                }

            }
        }
        return null;

    }
}
