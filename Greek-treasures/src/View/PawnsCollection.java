/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.*;
import Model.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author John
 */
public class PawnsCollection extends JFrame {

    private JPanel panel;
    private JButton arc1, arc2, arc3, thes;
    private Player player;
    int pathToSet, index;
    boolean hasChoose;
    private Controller control;
    private Board board;
    JButton but;

    public PawnsCollection(Player player, Controller control, int index, JButton but) {
        this.player = player;
        this.board = control.getBoard();
        this.pathToSet = player.getCard(index).getPath();
        this.index = index;
        this.but = but;
        this.control = control;
        this.hasChoose = false;
        this.setSize(500, 500);
        this.setVisible(true);
        this.setTitle("Select a pawn to place");
        panel = new JPanel();
        this.add(panel);
        for (int i = 0; i < 4; i++) {

            ImageIcon icon = new ImageIcon(player.getPawn(i).getSourcePath());
            Image img = icon.getImage();
            Image newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
            icon = new ImageIcon(newimg);
            switch (i) {
                case 0:
                    arc1 = new JButton();
                    arc1.addActionListener(new listener());
                    arc1.setIcon(icon);
                    panel.add(arc1);
                    if (!player.getPawn(0).canPlaced()) {
                        arc1.setVisible(false);
                    }

                    break;
                case 1:
                    arc2 = new JButton();
                    arc2.addActionListener(new listener());
                    arc2.setIcon(icon);
                    panel.add(arc2);
                    if (!player.getPawn(1).canPlaced()) {
                        arc2.setVisible(false);
                    }
                    break;
                case 2:
                    arc3 = new JButton();
                    arc3.addActionListener(new listener());
                    arc3.setIcon(icon);
                    panel.add(arc3);
                    if (!player.getPawn(2).canPlaced()) {
                        arc3.setVisible(false);
                    }
                    break;

                case 3:
                    thes = new JButton();
                    thes.addActionListener(new listener());
                    thes.setIcon(icon);
                    panel.add(thes);
                    if (!player.getPawn(3).canPlaced()) {
                        thes.setVisible(false);
                    }
                    break;

            }

        }

    }

    public class listener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            if (arc1 == (JButton) e.getSource()) {

                player.getPawn(0).setPath(pathToSet);
                player.getPawn(0).unplaceable();
                player.getPawn(0).setName("arc1");
                control.movePawn(index, player.getPawn(0), but);

                dispose();

            } else if (arc2 == (JButton) e.getSource()) {

                player.getPawn(1).setPath(pathToSet);
                player.getPawn(1).unplaceable();
                player.getPawn(1).setName("arc2");

                control.movePawn(index, player.getPawn(1), but);

                dispose();

            } else if (arc3 == (JButton) e.getSource()) {

                player.getPawn(2).setPath(pathToSet);
                player.getPawn(2).unplaceable();
                player.getPawn(2).setName("arc3");

                control.movePawn(index, player.getPawn(2), but);
                dispose();

            } else {
                player.getPawn(3).setPath(pathToSet);
                player.getPawn(3).unplaceable();
                player.getPawn(3).setName("thes");
                control.movePawn(index, player.getPawn(3), but);
                dispose();
            }

        }

    }

    public boolean hasChoose() {
        return this.hasChoose;
    }
}
