/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.awt.*;
import javax.swing.*;
import Model.*;
import Controller.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import static java.lang.Math.*;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author John
 */
public class Board extends JFrame {

    private JPanel p1_panel, p2_panel, info_panel, hand, score, findings, board_panel;
    public JLabel info, turn, p1_info, p2_info, p1_arc1, p1_arc2, p1_arc3, p1_thes, p2_arc1, p2_arc2, p2_arc3, p2_thes;
    private JButton stack, pl1_but1, pl1_but2, pl1_but3, pl1_but4, pl1_but5, pl1_but6, pl1_but7, pl1_but8, mural1;
    private JButton pl2_but1, pl2_but2, pl2_but3, pl2_but4, pl2_but5, pl2_but6, pl2_but7, pl2_but, pl2_but8, mural2;
    private JLabel knos1, mal1, phai1, zak1;
    private JLabel knos2, mal2, phai2, zak2, player1_info, player2_info;
    private backgroundPaint action_panel;
    private Image background;
    private Position board[][];
    private Findings find[][];
    private Controller control;
    private boolean discard;
    private backgroundPaint panels[][];

    /**
     * Constructs a new Board
     *
     * @param c
     * @throws IOException
     */
    public Board(Controller c) throws IOException {
        this.control = c;
        // this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        // this.setUndecorated(false);
        this.discard = false;
        this.setSize(1500, 1000);
        this.setVisible(true);
        this.setLayout(new BorderLayout());
        this.setTitle("Lost Minoan Ruins");
        this.setResizable(true);
        p1_panel = new JPanel();
        p2_panel = new JPanel();
        background = new ImageIcon("resources/images/background.jpg").getImage();
        action_panel = new backgroundPaint(background);
        action_panel.setLayout(new BorderLayout());
        info_panel = new JPanel();
        info_panel.setLayout(new BoxLayout(info_panel, BoxLayout.Y_AXIS));

        info = new JLabel();
        turn = new JLabel();
        info.setSize(new Dimension(200, 500));
        info.setText("Available Cards:" + c.getDeck().getAvailableCards());

        turn.setPreferredSize(new Dimension(200, 200));

        ImageIcon icon = new ImageIcon(c.getDeck().getPath());
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
        stack = new JButton(icon);
        stack.setMaximumSize(new Dimension(150, 240));
        stack.setPreferredSize(new Dimension(100, 150));
        turn.setMaximumSize(new Dimension(150, 150));
        info.setMaximumSize(new Dimension(150, 150));
        turn.setText("Available cards:");

        /*    Players    */
 /*     Player  1*/
        p1_panel.setLayout(new BorderLayout());
        hand = new JPanel();
        score = new JPanel();
        findings = new JPanel();

        //hand.setLayout(new GridLayout(1,2) );
        icon = new ImageIcon(c.getPlayer1().getCard(0).getSourcePath());
        img = icon.getImage();
        newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        pl1_but1 = new JButton(icon);
        icon = new ImageIcon(c.getPlayer1().getCard(1).getSourcePath());
        img = icon.getImage();
        newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        pl1_but2 = new JButton(icon);
        icon = new ImageIcon(c.getPlayer1().getCard(2).getSourcePath());
        img = icon.getImage();
        newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        pl1_but3 = new JButton(icon);
        icon = new ImageIcon(c.getPlayer1().getCard(3).getSourcePath());
        img = icon.getImage();
        newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        pl1_but4 = new JButton(icon);
        icon = new ImageIcon(c.getPlayer1().getCard(4).getSourcePath());
        img = icon.getImage();
        newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        pl1_but5 = new JButton(icon);
        icon = new ImageIcon(c.getPlayer1().getCard(5).getSourcePath());
        img = icon.getImage();
        newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        pl1_but6 = new JButton(icon);
        icon = new ImageIcon(c.getPlayer1().getCard(6).getSourcePath());
        img = icon.getImage();
        newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        pl1_but7 = new JButton(icon);
        icon = new ImageIcon(c.getPlayer1().getCard(7).getSourcePath());
        img = icon.getImage();
        newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        pl1_but8 = new JButton(icon);
        pl1_but1.setPreferredSize(new Dimension(100, 150));
        pl1_but2.setPreferredSize(new Dimension(100, 150));
        pl1_but3.setPreferredSize(new Dimension(100, 150));
        pl1_but4.setPreferredSize(new Dimension(100, 150));
        pl1_but5.setPreferredSize(new Dimension(100, 150));
        pl1_but6.setPreferredSize(new Dimension(100, 150));
        pl1_but7.setPreferredSize(new Dimension(100, 150));
        pl1_but8.setPreferredSize(new Dimension(100, 150));


        /*~~~~~Player Hand~~~~~~*/
        hand.add(pl1_but1);
        hand.add(pl1_but2);
        hand.add(pl1_but3);
        hand.add(pl1_but4);
        hand.add(pl1_but5);
        hand.add(pl1_but6);
        hand.add(pl1_but7);
        hand.add(pl1_but8);
        ImageIcon image_aux = new ImageIcon(new ImageIcon("resources/images/findings/ring.jpg").getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
        knos1 = new JLabel(image_aux);
        knos1.setVisible(false);
        image_aux = new ImageIcon(new ImageIcon("resources/images/findings/kosmima.jpg").getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
        mal1 = new JLabel(image_aux);
        mal1.setVisible(false);
        image_aux = new ImageIcon(new ImageIcon("resources/images/findings/diskos.jpg").getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
        phai1 = new JLabel(image_aux);
        phai1.setVisible(false);
        image_aux = new ImageIcon(new ImageIcon("resources/images/findings/ruto.jpg").getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
        zak1 = new JLabel(image_aux);
        zak1.setVisible(false);

        knos1.setPreferredSize(new Dimension(100, 100));
        mal1.setPreferredSize(new Dimension(100, 100));
        phai1.setPreferredSize(new Dimension(100, 100));
        zak1.setPreferredSize(new Dimension(100, 100));
        findings.add(knos1);
        findings.add(mal1);
        findings.add(phai1);
        findings.add(zak1);
        score.setLayout(new GridLayout(1, 2));
        mural1 = new JButton("My myrals");
        player1_info = new JLabel();
        player1_info.setText("<html>My score: " + c.getPlayer1().getPoints() + "<br>Murals Collected:<br>Path Counters:</html>");
        score.add(mural1);
        score.add(player1_info);
        p1_panel.add(hand, BorderLayout.CENTER);
        p1_panel.add(findings, BorderLayout.LINE_START);
        p1_panel.add(score, BorderLayout.LINE_END);

        /*   Player   2*/
        p2_panel.setLayout(new BorderLayout());
        hand = new JPanel();
        score = new JPanel();
        findings = new JPanel();

        //hand.setLayout(new GridLayout(1,2) );
        icon = new ImageIcon(c.getPlayer2().getCard(0).getSourcePath());
        img = icon.getImage();
        newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        pl2_but1 = new JButton(icon);
        icon = new ImageIcon(c.getPlayer2().getCard(1).getSourcePath());
        img = icon.getImage();
        newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        pl2_but2 = new JButton(icon);
        icon = new ImageIcon(c.getPlayer2().getCard(2).getSourcePath());
        img = icon.getImage();
        newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        pl2_but3 = new JButton(icon);
        icon = new ImageIcon(c.getPlayer2().getCard(3).getSourcePath());
        img = icon.getImage();
        newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        pl2_but4 = new JButton(icon);
        icon = new ImageIcon(c.getPlayer2().getCard(4).getSourcePath());
        img = icon.getImage();
        newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        pl2_but5 = new JButton(icon);
        icon = new ImageIcon(c.getPlayer2().getCard(5).getSourcePath());
        img = icon.getImage();
        newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        pl2_but6 = new JButton(icon);
        icon = new ImageIcon(c.getPlayer2().getCard(6).getSourcePath());
        img = icon.getImage();
        newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        pl2_but7 = new JButton(icon);
        icon = new ImageIcon(c.getPlayer2().getCard(7).getSourcePath());
        img = icon.getImage();
        newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        pl2_but8 = new JButton(icon);

        pl2_but1.setPreferredSize(new Dimension(100, 150));
        pl2_but2.setPreferredSize(new Dimension(100, 150));
        pl2_but3.setPreferredSize(new Dimension(100, 150));
        pl2_but4.setPreferredSize(new Dimension(100, 150));
        pl2_but5.setPreferredSize(new Dimension(100, 150));
        pl2_but6.setPreferredSize(new Dimension(100, 150));
        pl2_but7.setPreferredSize(new Dimension(100, 150));
        pl2_but8.setPreferredSize(new Dimension(100, 150));

        // System.out.println((c.getPlayer1().getCard(8).getSourcePath()));

        /*~~~~~Player Hand~~~~~~*/
        hand.add(pl2_but1);
        hand.add(pl2_but2);
        hand.add(pl2_but3);
        hand.add(pl2_but4);
        hand.add(pl2_but5);
        hand.add(pl2_but6);
        hand.add(pl2_but7);
        hand.add(pl2_but8);
        image_aux = new ImageIcon(new ImageIcon("resources/images/findings/ring.jpg").getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
        knos2 = new JLabel(image_aux);
        knos2.setVisible(false);
        image_aux = new ImageIcon(new ImageIcon("resources/images/findings/kosmima.jpg").getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
        mal2 = new JLabel(image_aux);
        mal2.setVisible(false);
        image_aux = new ImageIcon(new ImageIcon("resources/images/findings/diskos.jpg").getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
        phai2 = new JLabel(image_aux);
        phai2.setVisible(false);
        image_aux = new ImageIcon(new ImageIcon("resources/images/findings/ruto.jpg").getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
        zak2 = new JLabel(image_aux);
        zak2.setVisible(false);

        knos2.setPreferredSize(new Dimension(100, 100));
        mal2.setPreferredSize(new Dimension(100, 100));
        phai2.setPreferredSize(new Dimension(100, 100));
        zak2.setPreferredSize(new Dimension(100, 100));
        findings.add(knos2);
        findings.add(mal2);
        findings.add(phai2);
        findings.add(zak2);
        score.setLayout(new GridLayout(1, 2));
        mural2 = new JButton("My murals");
        // mural.setPreferredSize(new Dimension(50, 100));
        player2_info = new JLabel();
        player2_info.setText("<html>My score: " + c.getPlayer2().getPoints() + "<br>Murals Collected:<br>Path Counters:</html>");
        score.add(mural2);
        score.add(player2_info);
        p2_panel.add(hand, BorderLayout.CENTER);
        p2_panel.add(findings, BorderLayout.LINE_START);
        p2_panel.add(score, BorderLayout.LINE_END);

        /*    Action Board      */
 /*adding simple positions*/
        board = new Position[4][9];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 8; j = j + 2) {
                switch (i) {
                    case 0:
                        board[i][j] = new SimplePosition("Knossos", j);
                        // panels.add()
                        break;
                    case 1:
                        board[i][j] = new SimplePosition("Malia", j);
                        break;
                    case 2:
                        board[i][j] = new SimplePosition("Phaistos", j);
                        break;
                    case 3:
                        board[i][j] = new SimplePosition("Zakros", j);
                        break;
                }
                switch (j) {
                    case 0:
                        board[i][j].setPositionPoints(-20);
                        break;
                    case 1:
                        board[i][j].setPositionPoints(-15);
                        break;
                    case 2:
                        board[i][j].setPositionPoints(-10);
                        break;
                    case 3:
                        board[i][j].setPositionPoints(5);
                        break;
                    case 4:
                        board[i][j].setPositionPoints(10);
                        break;
                    case 5:
                        board[i][j].setPositionPoints(15);
                        break;
                    case 6:
                        board[i][j].setPositionPoints(30);
                        break;
                    case 7:
                        board[i][j].setPositionPoints(35);
                        break;
                    case 8:
                        board[i][j].setPositionPoints(50);
                        break;
                }

            }

        }

        /*adding findings position*/
        for (int i = 0; i < 4; i++) {
            for (int j = 1; j < 9; j = j + 2) {
                switch (i) {
                    case 0:
                        board[i][j] = new FindingsPosition("KnossosFind", j);
                        break;
                    case 1:
                        board[i][j] = new FindingsPosition("MaliaFind", j);
                        break;
                    case 2:
                        board[i][j] = new FindingsPosition("PhaistosFind", j);
                        break;
                    case 3:
                        board[i][j] = new FindingsPosition("ZakrosFind", j);
                        break;
                }
                switch (j) {
                    case 0:
                        board[i][j].setPositionPoints(-20);
                        break;
                    case 1:
                        board[i][j].setPositionPoints(-15);
                        break;
                    case 2:
                        board[i][j].setPositionPoints(-10);
                        break;
                    case 3:
                        board[i][j].setPositionPoints(5);
                        break;
                    case 4:
                        board[i][j].setPositionPoints(10);
                        break;
                    case 5:
                        board[i][j].setPositionPoints(15);
                        break;
                    case 6:
                        board[i][j].setPositionPoints(30);
                        break;
                    case 7:
                        board[i][j].setPositionPoints(35);
                        break;
                    case 8:
                        board[i][j].setPositionPoints(50);
                        break;
                }
            }

        }

        /* adding final*/
        for (int i = 0; i < 4; i++) {
            switch (i) {
                case 0:
                    board[i][8] = new FindingsPosition("KnossosPalace", 8);
                    break;
                case 1:
                    board[i][8] = new FindingsPosition("MaliaPalace", 8);
                    break;
                case 2:
                    board[i][8] = new FindingsPosition("PhaistosPalace", 8);
                    break;
                case 3:
                    board[i][8] = new FindingsPosition("ZakrosPalace", 8);
                    break;
            }
            board[i][8].setPositionPoints(50);

        }
        board_panel = new JPanel();
        board_panel.setOpaque(false);
        board_panel.setLayout(new GridLayout(5, 9, 10, 45));
        for (int i = 0; i < 9; i++) {
            JLabel pts = new JLabel();
            switch (i) {
                case 0:
                    pts = new JLabel("-20 points");

                    break;
                case 1:
                    pts = new JLabel("-15 points");
                    break;
                case 2:
                    pts = new JLabel("-10 points");
                    break;
                case 3:
                    pts = new JLabel("5 points");

                    break;
                case 4:
                    pts = new JLabel("10 points");
                    break;
                case 5:
                    pts = new JLabel("15 points");
                    break;
                case 6:
                    pts = new JLabel("<html>30 points" + "<br>CHECK POINT!:</html>");
                    break;
                case 7:
                    pts = new JLabel("35 points");
                    break;
                case 8:
                    pts = new JLabel("50 points");
                    break;

            }
            board_panel.add(pts);

        }
        panels = new backgroundPaint[4][9];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 9; j++) {
                //  board_panel.add(new backgroundPaint(new ImageIcon(board[i][j].getSourcePath()).getImage()));
                panels[i][j] = (new backgroundPaint(new ImageIcon(board[i][j].getSourcePath()).getImage()));
                board_panel.add(panels[i][j]);
            }
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 9; j++) {
                switch (j) {
                    case 0:
                        board[i][j].setPositionPoints(-20);

                        break;
                    case 1:
                        board[i][j].setPositionPoints(-15);
                        break;
                    case 2:
                        board[i][j].setPositionPoints(-10);
                        break;
                    case 3:
                        board[i][j].setPositionPoints(5);

                        break;
                    case 4:
                        board[i][j].setPositionPoints(10);
                        break;
                    case 5:
                        board[i][j].setPositionPoints(15);
                        break;
                    case 6:
                        board[i][j].setPositionPoints(30);
                        break;
                    case 7:
                        board[i][j].setPositionPoints(35);
                        break;
                    case 8:
                        board[i][j].setPositionPoints(50);
                        break;

                }
            }
        }


        /*    Setting findings   */
 /*Rare Findings  */
        find = new Findings[4][9];
        Random random = new Random();
        int pos;
        pos = random.nextInt(8 - 0 + 1) + 0;
        while (!(board[0][pos] instanceof FindingsPosition)) {
            pos = random.nextInt(8 - 0 + 1) + 0;
        }
        find[0][pos] = new RareFinding(0);
        pos = random.nextInt(8 - 0 + 1) + 0;
        while (!(board[1][pos] instanceof FindingsPosition)) {
            pos = random.nextInt(8 - 0 + 1) + 0;
        }
        find[1][pos] = new RareFinding(1);
        pos = random.nextInt(8 - 0 + 1) + 0;
        while (!(board[2][pos] instanceof FindingsPosition)) {
            pos = random.nextInt(8 - 0 + 1) + 0;
        }
        find[2][pos] = new RareFinding(2);
        pos = random.nextInt(8 - 0 + 1) + 0;
        while (!(board[3][pos] instanceof FindingsPosition)) {
            pos = random.nextInt(8 - 0 + 1) + 0;
        }
        find[3][pos] = new RareFinding(3);

        /*     murals,statues*/
        int statues = 0, murals = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 9; j++) {
                if (board[i][j] instanceof FindingsPosition && find[i][j] == null) {
                    Statue new_statue = new Statue(i);
                    Murals new_mural = new Murals(i, (murals + 1));
                    if ((statues < 12) && (murals < 6)) {
                        int r = random.nextInt(2);
                        if (r == 1) {
                            find[i][j] = new_statue;
                            statues++;

                        } else {
                            find[i][j] = new_mural;
                            murals++;
                        }
                    } else if (statues < 12) {
                        find[i][j] = new_statue;
                        statues++;
                    } else if (murals < 6) {
                        find[i][j] = new_mural;
                        murals++;
                    }

                }

            }
        }
      

       /* for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 9; j++) {
                if (find[i][j] instanceof RareFinding) {
                    System.out.println("Rare at[" + i + "]" + "[" + j + "]");
                } else if (find[i][j] instanceof Statue) {
                    System.out.println("Statue at[" + i + "]" + "[" + j + "]");
                } else if (find[i][j] instanceof Murals) {
                    System.out.println("Mural at[" + i + "]" + "[" + j + "]");
                }
            }
        }*/

        /*    Pawns   */
        ImageIcon image_arch1 = new ImageIcon(new ImageIcon(c.getPlayer1().getPawn(0).getSourcePath()).getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
        ImageIcon image_thes1 = new ImageIcon(new ImageIcon(c.getPlayer1().getPawn(3).getSourcePath()).getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
        ImageIcon image_arch2 = new ImageIcon(new ImageIcon(c.getPlayer2().getPawn(0).getSourcePath()).getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
        ImageIcon image_thes2 = new ImageIcon(new ImageIcon(c.getPlayer2().getPawn(3).getSourcePath()).getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
        p1_arc1 = new JLabel(image_arch1);
        p1_arc2 = new JLabel(image_arch1);
        p1_arc3 = new JLabel(image_arch1);
        p1_thes = new JLabel(image_thes1);
        p2_arc1 = new JLabel(image_arch2);
        p2_arc2 = new JLabel(image_arch2);
        p2_arc3 = new JLabel(image_arch2);
        p2_thes = new JLabel(image_thes2);
        turn.setOpaque(true);
        info.setOpaque(true);

        info_panel.setOpaque(false);
        // info_panel.add();
        //info_panel.add(info);
        info_panel.add(stack);
        //info_panel.add(turn);
        info_panel.add(info);
        info_panel.setPreferredSize(new Dimension(500, 100));
        action_panel.add(info_panel, BorderLayout.LINE_START);
        action_panel.add(board_panel, BorderLayout.CENTER);

        /* Action Listeners  */
        pl1_but1.addActionListener(new listener());
        pl1_but2.addActionListener(new listener());
        pl1_but3.addActionListener(new listener());
        pl1_but4.addActionListener(new listener());
        pl1_but5.addActionListener(new listener());
        pl1_but6.addActionListener(new listener());
        pl1_but7.addActionListener(new listener());
        pl1_but8.addActionListener(new listener());
        mural1.addActionListener(new listener());
        mural2.addActionListener(new listener());
        pl2_but1.addActionListener(new listener());
        pl2_but2.addActionListener(new listener());
        pl2_but3.addActionListener(new listener());
        pl2_but4.addActionListener(new listener());
        pl2_but5.addActionListener(new listener());
        pl2_but6.addActionListener(new listener());
        pl2_but7.addActionListener(new listener());
        pl2_but8.addActionListener(new listener());
        stack.addActionListener(new listener());

        this.add(p1_panel, BorderLayout.PAGE_START);
        this.add(p2_panel, BorderLayout.PAGE_END);
        this.add(action_panel, BorderLayout.CENTER);
        this.setVisible(true);
        //this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public class listener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            int path, aux;
            Pawn pawn;
            if (mural1 == (JButton) e.getSource()) {

                MuralsCollection col1 = new MuralsCollection(control.getPlayer1());

            }
            if (mural2 == (JButton) e.getSource()) {

                MuralsCollection col2 = new MuralsCollection(control.getPlayer2());
                
            }
            if (stack == (JButton) e.getSource()) {
                if (control.getPlayer1().getTurn()) {
                    if (control.getPlayer1().getDiscard()) {
                        control.getPlayer1().setDiscard(false);

                    } else {
                        control.getPlayer1().setDiscard(true);

                    }
                }
                if (control.getPlayer2().getTurn()) {
                    if (control.getPlayer2().getDiscard()) {
                        control.getPlayer2().setDiscard(false);
                    } else {
                        control.getPlayer2().setDiscard(true);
                    }
                }
            }

            if (pl1_but1 == (JButton) e.getSource()) {
                if (control.getPlayer1().getTurn() && control.getPlayer1().getDiscard() == false) {

                    path = control.getPlayer1().getCard(0).getPath();
                    if (control.getPlayer1().getCard(0) instanceof MinotaurCard) {
                        aux = control.comparingPaths(path, control.getPlayer2());
                        if (aux != -1) {

                            control.movePawn(0, control.getPlayer2().getPawn(aux), pl1_but1);
                        }
                    } else {

                        if (control.getPlayer1().getPath(path) == -1) {
                            if (control.getPlayer1().getCard(0) instanceof NumericCard) {

                                control.getPlayer1().setPath(path);

                                control.moveNew(0, pl1_but1);
                            }

                        } else {
                            if (control.getPlayer1().getCard(0) instanceof NumericCard || control.getPlayer1().getCard(0) instanceof AriadneCard) {

                                aux = control.comparingPaths(path, control.getPlayer1());
                                control.movePawn(0, control.getPlayer1().getPawn(aux), pl1_but1);
                            }
                        }
                    }

                } else if (control.getPlayer1().getTurn() && control.getPlayer1().getDiscard()) {
                    control.discardCard(0, pl1_but1);
                }
            } else if (pl1_but2 == (JButton) e.getSource()) {
                if (control.getPlayer1().getTurn() && control.getPlayer1().getDiscard() == false) {

                    path = control.getPlayer1().getCard(1).getPath();
                    if (control.getPlayer1().getCard(1) instanceof MinotaurCard) {
                        aux = control.comparingPaths(path, control.getPlayer2());
                        if (aux != -1) {

                            control.movePawn(1, control.getPlayer2().getPawn(aux), pl1_but2);
                        }
                    } else {
                        if (control.getPlayer1().getPath(path) == -1) {
                            if (control.getPlayer1().getCard(1) instanceof NumericCard) {

                                control.moveNew(1, pl1_but2);

                                control.getPlayer1().setPath(path);
                            }
                        } else {
                            if (control.getPlayer1().getCard(1) instanceof NumericCard || control.getPlayer1().getCard(1) instanceof AriadneCard) {

                                aux = control.comparingPaths(path, control.getPlayer1());
                                control.movePawn(1, control.getPlayer1().getPawn(aux), pl1_but2);
                            }
                        }
                    }

                } else if (control.getPlayer1().getTurn() && control.getPlayer1().getDiscard()) {
                    if (control.getPlayer1().getTurn()) {

                    }
                    control.discardCard(1, pl1_but2);
                }
            } else if (pl1_but3 == (JButton) e.getSource()) {
                if (control.getPlayer1().getTurn() && control.getPlayer1().getDiscard() == false) {

                    path = control.getPlayer1().getCard(2).getPath();
                    if (control.getPlayer1().getCard(2) instanceof MinotaurCard) {
                        aux = control.comparingPaths(path, control.getPlayer2());
                        if (aux != -1) {
                            control.movePawn(2, control.getPlayer2().getPawn(aux), pl1_but3);
                        }
                    } else {
                        if (control.getPlayer1().getPath(path) == -1) {
                            if (control.getPlayer1().getCard(2) instanceof NumericCard) {

                                control.moveNew(2, pl1_but3);

                                control.getPlayer1().setPath(path);
                            }
                        } else {
                            if (control.getPlayer1().getCard(2) instanceof NumericCard || control.getPlayer1().getCard(2) instanceof AriadneCard) {

                                aux = control.comparingPaths(path, control.getPlayer1());

                                control.movePawn(2, control.getPlayer1().getPawn(aux), pl1_but3);
                            }
                        }
                    }

                } else if (control.getPlayer1().getTurn() && control.getPlayer1().getDiscard()) {
                    if (control.getPlayer1().getTurn()) {

                    }
                    control.discardCard(2, pl1_but3);
                }
            } else if (pl1_but4 == (JButton) e.getSource()) {
                if (control.getPlayer1().getTurn() && control.getPlayer1().getDiscard() == false) {

                    path = control.getPlayer1().getCard(3).getPath();
                    if (control.getPlayer1().getCard(3) instanceof MinotaurCard) {
                        aux = control.comparingPaths(path, control.getPlayer2());
                        if (aux != -1) {

                            control.movePawn(3, control.getPlayer2().getPawn(aux), pl1_but4);
                        }
                    } else {
                        if (control.getPlayer1().getPath(path) == -1) {
                            if (control.getPlayer1().getCard(3) instanceof NumericCard) {
                                control.moveNew(3, pl1_but4);

                                control.getPlayer1().setPath(path);
                            }

                        } else {
                            if (control.getPlayer1().getCard(3) instanceof NumericCard || control.getPlayer1().getCard(3) instanceof AriadneCard) {
                              
                                aux = control.comparingPaths(path, control.getPlayer1());
                                control.movePawn(3, control.getPlayer1().getPawn(aux), pl1_but4);
                            }
                        }
                    }

                } else if (control.getPlayer1().getTurn() && control.getPlayer1().getDiscard()) {
                    if (control.getPlayer1().getTurn()) {

                    }
                    control.discardCard(3, pl1_but4);
                }
            } else if (pl1_but5 == (JButton) e.getSource()) {
                if (control.getPlayer1().getTurn() && control.getPlayer1().getDiscard() == false) {

                    path = control.getPlayer1().getCard(4).getPath();
                    if (control.getPlayer1().getCard(4) instanceof MinotaurCard) {
                        aux = control.comparingPaths(path, control.getPlayer2());
                        if (aux != -1) {

                            control.movePawn(4, control.getPlayer2().getPawn(aux), pl1_but5);
                        }
                    } else {
                        if (control.getPlayer1().getPath(path) == -1) {
                            if (control.getPlayer1().getCard(4) instanceof NumericCard) {

                                control.moveNew(4, pl1_but5);

                                control.getPlayer1().setPath(path);
                            }
                        } else {
                            if (control.getPlayer1().getCard(4) instanceof NumericCard || control.getPlayer1().getCard(4) instanceof AriadneCard) {

                                aux = control.comparingPaths(path, control.getPlayer1());

                                control.movePawn(4, control.getPlayer1().getPawn(aux), pl1_but5);
                            }
                        }
                    }

                } else if (control.getPlayer1().getTurn() && control.getPlayer1().getDiscard()) {
                    if (control.getPlayer1().getTurn()) {

                    }
                    control.discardCard(4, pl1_but5);
                }
            } else if (pl1_but6 == (JButton) e.getSource()) {
                if (control.getPlayer1().getTurn() && control.getPlayer1().getDiscard() == false) {

                    path = control.getPlayer1().getCard(5).getPath();
                    if (control.getPlayer1().getCard(5) instanceof MinotaurCard) {
                        aux = control.comparingPaths(path, control.getPlayer2());
                        if (aux != -1) {

                            control.movePawn(5, control.getPlayer2().getPawn(aux), pl1_but6);
                        }
                    } else {
                        if (control.getPlayer1().getPath(path) == -1) {
                            if (control.getPlayer1().getCard(5) instanceof NumericCard) {

                                control.moveNew(5, pl1_but6);

                                control.getPlayer1().setPath(path);
                            }
                        } else {
                            if (control.getPlayer1().getCard(5) instanceof NumericCard || control.getPlayer1().getCard(5) instanceof AriadneCard) {

                                aux = control.comparingPaths(path, control.getPlayer1());

                                control.movePawn(5, control.getPlayer1().getPawn(aux), pl1_but6);
                            }
                        }
                    }

                } else if (control.getPlayer1().getTurn() && control.getPlayer1().getDiscard()) {
                    if (control.getPlayer1().getTurn()) {

                    }
                    control.discardCard(5, pl1_but6);
                }
            } else if (pl1_but7 == (JButton) e.getSource()) {
                if (control.getPlayer1().getTurn() && control.getPlayer1().getDiscard() == false) {

                    path = control.getPlayer1().getCard(6).getPath();
                    if (control.getPlayer1().getCard(6) instanceof MinotaurCard) {
                        aux = control.comparingPaths(path, control.getPlayer2());
                        if (aux != -1) {

                            control.movePawn(6, control.getPlayer2().getPawn(aux), pl1_but7);
                        }
                    } else {
                        if (control.getPlayer1().getPath(path) == -1) {
                            if (control.getPlayer1().getCard(6) instanceof NumericCard) {

                                control.moveNew(6, pl1_but7);

                                control.getPlayer1().setPath(path);
                            }
                        } else {
                            if (control.getPlayer1().getCard(6) instanceof NumericCard || control.getPlayer1().getCard(6) instanceof AriadneCard) {

                                aux = control.comparingPaths(path, control.getPlayer1());

                                control.movePawn(6, control.getPlayer1().getPawn(aux), pl1_but7);
                            }
                        }
                    }

                } else if (control.getPlayer1().getTurn() && control.getPlayer1().getDiscard()) {
                    if (control.getPlayer1().getTurn()) {

                    }
                    control.discardCard(6, pl1_but7);
                }
            } else if (pl1_but8 == (JButton) e.getSource()) {
                if (control.getPlayer1().getTurn() && control.getPlayer1().getDiscard() == false) {

                    path = control.getPlayer1().getCard(7).getPath();
                    if (control.getPlayer1().getCard(7) instanceof MinotaurCard) {
                        aux = control.comparingPaths(path, control.getPlayer2());
                        if (aux != -1) {

                            control.movePawn(7, control.getPlayer2().getPawn(aux), pl1_but8);
                        }
                    } else {
                        if (control.getPlayer1().getPath(path) == -1) {
                            if (control.getPlayer1().getCard(7) instanceof NumericCard) {

                                control.moveNew(7, pl1_but8);

                                control.getPlayer1().setPath(path);
                            }
                        } else {
                            if (control.getPlayer1().getCard(7) instanceof NumericCard || control.getPlayer1().getCard(7) instanceof AriadneCard) {

                                aux = control.comparingPaths(path, control.getPlayer1());

                                control.movePawn(7, control.getPlayer1().getPawn(aux), pl1_but8);
                            }
                        }
                    }

                } else if (control.getPlayer1().getTurn() && control.getPlayer1().getDiscard()) {
                    if (control.getPlayer1().getTurn()) {

                    }
                    control.discardCard(7, pl1_but8);
                }
            }

            if (pl2_but1 == (JButton) e.getSource()) {

                if (control.getPlayer2().getTurn() && control.getPlayer2().getDiscard() == false) {

                    path = control.getPlayer2().getCard(0).getPath();
                    if (control.getPlayer2().getCard(0) instanceof MinotaurCard) {
                        aux = control.comparingPaths(path, control.getPlayer1());
                        if (aux != -1) {

                            control.movePawn(0, control.getPlayer1().getPawn(aux), pl2_but1);
                        }
                    } else {

                        if (control.getPlayer2().getPath(path) == -1) {
                            if (control.getPlayer2().getCard(0) instanceof NumericCard) {

                                control.getPlayer2().setPath(path);
                                control.moveNew(0, pl2_but1);
                            }

                        } else {

                            aux = control.comparingPaths(path, control.getPlayer2());

                            control.movePawn(0, control.getPlayer2().getPawn(aux), pl2_but1);
                        }
                    }
                } else if (control.getPlayer2().getTurn() && control.getPlayer2().getDiscard()) {
                    if (control.getPlayer2().getTurn()) {

                    }
                    control.discardCard(0, pl2_but1);
                }

            } else if (pl2_but2 == (JButton) e.getSource()) {
                if (control.getPlayer2().getTurn() && control.getPlayer2().getDiscard() == false) {

                    path = control.getPlayer2().getCard(1).getPath();
                    if (control.getPlayer2().getCard(1) instanceof MinotaurCard) {
                        aux = control.comparingPaths(path, control.getPlayer1());
                        if (aux != -1) {

                            control.movePawn(1, control.getPlayer1().getPawn(aux), pl2_but2);
                        }
                    } else {
                        if (control.getPlayer2().getPath(path) == -1) {
                            if (control.getPlayer2().getCard(1) instanceof NumericCard) {

                                control.moveNew(1, pl2_but2);
                                control.getPlayer2().setPath(path);
                            }
                        } else {
                            aux = control.comparingPaths(path, control.getPlayer2());

                            control.movePawn(1, control.getPlayer2().getPawn(aux), pl2_but2);
                        }
                    }
                } else if (control.getPlayer2().getTurn() && control.getPlayer2().getDiscard()) {
                    if (control.getPlayer2().getTurn()) {

                    }
                    control.discardCard(1, pl2_but2);
                }

            } else if (pl2_but3 == (JButton) e.getSource()) {
                if (control.getPlayer2().getTurn() && control.getPlayer2().getDiscard() == false) {

                    path = control.getPlayer2().getCard(2).getPath();
                    if (control.getPlayer2().getCard(2) instanceof MinotaurCard) {
                        aux = control.comparingPaths(path, control.getPlayer1());
                        if (aux != -1) {

                            control.movePawn(2, control.getPlayer1().getPawn(aux), pl2_but3);
                        }
                    } else {
                        if (control.getPlayer2().getPath(path) == -1) {
                            if (control.getPlayer2().getCard(2) instanceof NumericCard) {
                                if (control.getPlayer2().getTurn()) {
                                    control.moveNew(2, pl2_but3);
                                    control.getPlayer2().setPath(path);
                                }
                            }
                        } else {

                            aux = control.comparingPaths(path, control.getPlayer2());

                            control.movePawn(2, control.getPlayer2().getPawn(aux), pl2_but3);
                        }

                    }
                } else if (control.getPlayer2().getTurn() && control.getPlayer2().getDiscard()) {
                    if (control.getPlayer2().getTurn()) {

                    }
                    control.discardCard(2, pl2_but3);
                }
            } else if (pl2_but4 == (JButton) e.getSource()) {
                if (control.getPlayer2().getTurn() && control.getPlayer2().getDiscard() == false) {

                    path = control.getPlayer2().getCard(3).getPath();
                    if (control.getPlayer2().getCard(3) instanceof MinotaurCard) {
                        aux = control.comparingPaths(path, control.getPlayer1());
                        if (aux != -1) {

                            control.movePawn(3, control.getPlayer1().getPawn(aux), pl2_but4);
                        }
                    } else {
                        if (control.getPlayer2().getPath(path) == -1) {
                            if (control.getPlayer2().getCard(3) instanceof NumericCard) {

                                control.moveNew(3, pl2_but4);
                                control.getPlayer2().setPath(path);
                            }
                        } else {

                            aux = control.comparingPaths(path, control.getPlayer2());

                            control.movePawn(3, control.getPlayer2().getPawn(aux), pl2_but4);
                        }

                    }
                } else if (control.getPlayer2().getTurn() && control.getPlayer2().getDiscard()) {
                    if (control.getPlayer2().getTurn()) {

                    }
                    control.discardCard(3, pl2_but4);
                }
            } else if (pl2_but5 == (JButton) e.getSource()) {
                if (control.getPlayer2().getTurn() && control.getPlayer2().getDiscard() == false) {

                    path = control.getPlayer2().getCard(4).getPath();
                    if (control.getPlayer2().getCard(4) instanceof MinotaurCard) {
                        aux = control.comparingPaths(path, control.getPlayer1());
                        if (aux != -1) {

                            control.movePawn(4, control.getPlayer1().getPawn(aux), pl2_but5);
                        }
                    } else {
                        if (control.getPlayer2().getPath(path) == -1) {
                            if (control.getPlayer2().getCard(4) instanceof NumericCard) {

                                control.moveNew(4, pl2_but5);
                                control.getPlayer2().setPath(path);
                            }
                        } else {

                            aux = control.comparingPaths(path, control.getPlayer2());

                            control.movePawn(4, control.getPlayer2().getPawn(aux), pl2_but5);
                        }

                    }
                } else if (control.getPlayer2().getTurn() && control.getPlayer2().getDiscard()) {
                    if (control.getPlayer2().getTurn()) {

                    }
                    control.discardCard(4, pl2_but5);
                }
            } else if (pl2_but6 == (JButton) e.getSource()) {
                if (control.getPlayer2().getTurn() && control.getPlayer2().getDiscard() == false) {

                    path = control.getPlayer2().getCard(5).getPath();
                    if (control.getPlayer2().getCard(5) instanceof MinotaurCard) {
                        aux = control.comparingPaths(path, control.getPlayer1());
                        if (aux != -1) {
                         
                            control.movePawn(5, control.getPlayer1().getPawn(aux), pl2_but6);
                        }
                    } else {
                        if (control.getPlayer2().getPath(path) == -1) {
                            if (control.getPlayer2().getCard(5) instanceof NumericCard) {

                                control.moveNew(5, pl2_but6);
                                control.getPlayer2().setPath(path);
                            }
                        } else {

                            aux = control.comparingPaths(path, control.getPlayer2());
                            control.movePawn(5, control.getPlayer2().getPawn(aux), pl2_but6);
                        }

                    }
                } else if (control.getPlayer2().getTurn() && control.getPlayer2().getDiscard()) {
                    if (control.getPlayer2().getTurn()) {

                    }
                    control.discardCard(5, pl2_but6);
                }
            } else if (pl2_but7 == (JButton) e.getSource()) {
                if (control.getPlayer2().getTurn() && control.getPlayer2().getDiscard() == false) {

                    path = control.getPlayer2().getCard(6).getPath();
                    if (control.getPlayer2().getCard(6) instanceof MinotaurCard) {
                        aux = control.comparingPaths(path, control.getPlayer1());
                        if (aux != -1) {

                            control.movePawn(6, control.getPlayer1().getPawn(aux), pl2_but7);
                        }
                    } else {
                        if (control.getPlayer2().getPath(path) == -1) {
                            if (control.getPlayer2().getCard(6) instanceof NumericCard) {

                                control.moveNew(6, pl2_but7);
                                control.getPlayer2().setPath(path);
                            }
                        } else {

                            aux = control.comparingPaths(path, control.getPlayer2());

                            control.movePawn(6, control.getPlayer2().getPawn(aux), pl2_but7);
                        }

                    }
                } else if (control.getPlayer2().getTurn() && control.getPlayer2().getDiscard()) {
                    if (control.getPlayer2().getTurn()) {

                    }
                    control.discardCard(6, pl2_but7);
                }
            } else if (pl2_but8 == (JButton) e.getSource()) {
                if (control.getPlayer2().getTurn() && control.getPlayer2().getDiscard() == false) {

                    if (control.getDeck().canDraw()) {
                        path = control.getPlayer2().getCard(7).getPath();
                        if (control.getPlayer2().getCard(7) instanceof MinotaurCard) {
                            aux = control.comparingPaths(path, control.getPlayer1());
                            if (aux != -1) {

                                control.movePawn(7, control.getPlayer1().getPawn(aux), pl2_but8);
                            }
                        } else {

                            if (control.getPlayer2().getPath(path) == -1) {
                                if (control.getPlayer2().getCard(7) instanceof NumericCard) {

                                    control.moveNew(7, pl2_but8);
                                    control.getPlayer2().setPath(path);
                                }
                            } else {
                                aux = control.comparingPaths(path, control.getPlayer2());

                                control.movePawn(7, control.getPlayer2().getPawn(aux), pl2_but8);
                            }

                        }
                    }
                } else if (control.getPlayer2().getTurn() && control.getPlayer2().getDiscard()) {
                    if (control.getPlayer2().getTurn()) {

                    }
                    control.discardCard(7, pl2_but8);
                }
            }

        }
    }

    /**
     * Updates the players hand.
     *
     * @param c
     * @param card
     */

    public void updateHand(Card c, JButton card) {
        ImageIcon icon = new ImageIcon(c.getSourcePath());
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        card.setIcon(icon);
    }

    /**
     * Updates the board
     *
     * @param c
     * @param path
     * @param position
     * @param card
     */
    public void updateBoard(Card c, int path, int position, JButton card) {
        /*   Updating Hand   */

        ImageIcon icon = new ImageIcon(c.getSourcePath());
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(100, 150, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        card.setIcon(icon);

        /*    Updating Board   */
        if (control.getPlayer1().getTurn()) {
            JLabel pawn = pathToPawn(path, control.getPlayer1());
            switch (path) {
                case 0:
                    this.panels[path][position].add(pawn);
                    break;
                case 1:
                    this.panels[path][position].add(pawn);
                    break;
                case 2:
                    this.panels[path][position].add(pawn);
                    break;
                case 3:
                    this.panels[path][position].add(pawn);
                    break;
            }
        } else {
            JLabel pawn = pathToPawn(path, control.getPlayer2());

            switch (path) {
                case 0:
                    this.panels[path][position].add(pawn);
                    break;
                case 1:
                    this.panels[path][position].add(pawn);
                    break;
                case 2:
                    this.panels[path][position].add(pawn);
                    break;
                case 3:
                    this.panels[path][position].add(pawn);
                    break;
            }
        }

        this.repaint();
        this.validate();

    }

    /**
     * Updates the infoBox
     *
     * @param p
     */
    public void updateInfo(Player p) {
        String s;
        int statues = p.getStatues().size();
        if (p == control.getPlayer1()) {
            player1_info.setText("<html>My score: " + p.getPoints() + "<br>Statues Collected:" + statues + "<br>Path Counters:" + control.getPathCounter(p) + "</html>");
            s = "Turn Player 2";
        } else {
            player2_info.setText("<html>My score: " + p.getPoints() + "<br>Statues Collected:" + statues + "<br>Path Counters:" + control.getPathCounter(p) + "</html>");
            s = "Turn Player 1";
        }
        info.setText("<html>Available cards: " + control.getDeck().getAvailableCards() + "<br>" + s + "<br>Check points:" + control.getCheckPoints() + "</html>");

    }

    public backgroundPaint getPanel(int i, int j) {
        return panels[i][j];
    }

    public Findings getFindingPosition(int i, int j) {
        return find[i][j];

    }

    /**
     * Updates the Rare's view.
     *
     * @param p
     * @param path
     */
    public void updateRare(Player p, int path) {
        if (p == control.getPlayer1()) {
            switch (path) {
                case 0:
                    knos1.setVisible(true);
                    break;
                case 1:
                    mal1.setVisible(true);
                    break;
                case 2:
                    phai1.setVisible(true);
                    break;
                case 3:
                    zak1.setVisible(true);
                    break;

            }
        }

        if (p == control.getPlayer2()) {
            switch (path) {
                case 0:
                    knos2.setVisible(true);
                    break;
                case 1:
                    mal2.setVisible(true);
                    break;
                case 2:
                    phai2.setVisible(true);
                    break;
                case 3:
                    zak2.setVisible(true);
                    break;

            }
        }
    }

    /**
     * Given a path,returns a player's corresponding view of a pawn.
     *
     * @param path
     * @param player
     * @return
     */

    public JLabel pathToPawn(int path, Player player) {
        if (control.getPlayer1().getTurn()) {
            if (path == player.getPawn(0).getPath()) {
                return p1_arc1;
            } else if (path == player.getPawn(1).getPath()) {
                return p1_arc2;
            } else if (path == player.getPawn(2).getPath()) {
                return p1_arc3;
            } else {
                return p1_thes;
            }
        } else {

            if (path == player.getPawn(0).getPath()) {
                return p2_arc1;
            } else if (path == player.getPawn(1).getPath()) {
                return p2_arc2;
            } else if (path == player.getPawn(2).getPath()) {
                return p2_arc3;
            } else {
                return p2_thes;
            }
        }

    }

    public Position getBoardPosition(int i, int j) {
        return this.board[i][j];
    }

    /**
     * Auxiliary function for debugging.
     */
    public void printPoints() {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 9; j++) {
                System.out.print("[" + board[i][j].getPositionPoints() + "] ");
            }
            System.out.println();
        }
    }

}
