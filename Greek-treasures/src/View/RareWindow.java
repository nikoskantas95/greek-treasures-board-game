/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

/**
 *
 * @author John
 */
import Model.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import javax.swing.JFrame;

public class RareWindow extends JFrame {

    private JPanel panel;

    private ImageIcon icon;
    private Image img, newimg;
    private JLabel photo, info;

    public RareWindow(RareFinding rare) {
        this.setSize(1000, 300);
        this.setTitle(rare.getMessage());
        this.setVisible(true);
        this.panel = new JPanel();
        panel.setLayout(new GridLayout(2, 1));
        this.photo = new JLabel(new ImageIcon(rare.getSourcePath()));      
        this.icon = new ImageIcon(rare.getSourcePath());
        img = icon.getImage();
        newimg = img.getScaledInstance(150, 100, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        this.photo.setIcon(icon);
        info = new JLabel(rare.getDescription());
        info.setVisible(true);
        panel.add(photo);
        panel.add(info);
        this.add(panel);
    }

}
