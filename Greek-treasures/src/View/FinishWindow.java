/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.*;

import javax.swing.JFrame;

public class FinishWindow extends JFrame {

    private JPanel pan;
    private JLabel lab, info;

    /**
     * Constructs a FinishWindow
     *
     * @param path
     * @throws FileNotFoundException
     * @throws IOException
     */

    public FinishWindow(int path) throws FileNotFoundException, IOException {
        this.setSize(new Dimension(500, 500));
        this.setVisible(true);
      
        // this.setResizable(false);  
        pan = new JPanel();
        String b[][] = new String[16][3];
        String splitBy = ",";
        BufferedReader br = new BufferedReader(new FileReader("resources/csvFiles/csv_greeklish.csv"));
        String line = br.readLine();
        int i = 0;
        while ((line = br.readLine()) != null) {
            b[i++] = line.split(splitBy);

        }
        br.close();
        switch (path) {
            case 0:
                this.setTitle(b[11][1]);
                lab = new JLabel(b[11][2]);
                info = new JLabel(new ImageIcon("resources/images/paths/knossosPalace.jpg"));
                break;
            case 1:
                this.setTitle(b[13][1]);
                lab = new JLabel(b[13][2]);
                info = new JLabel(new ImageIcon("resources/images/paths/maliaPalace.jpg"));
                break;
            case 2:
                this.setTitle(b[12][1]);
                lab = new JLabel(b[12][2]);
                info = new JLabel(new ImageIcon("resources/images/paths/phaistosPalace.jpg"));
                break;
            case 3:
                this.setTitle(b[14][1]);
                lab = new JLabel(b[14][2]);
                info = new JLabel(new ImageIcon("resources/images/paths/zakrosPalace.jpg"));
                break;

        }

        pan.add(info);
        pan.add(lab);
        this.add(pan);
        //this.add(info);
    }
}
