/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author John
 */
public class MuralsCollection extends JFrame {
    
    private JLabel icon;
    private JPanel panel;
    

    public MuralsCollection(Player player) {
        this.setSize(600, 400);
        this.setTitle("My murals collection");
        this.setVisible(true);
        panel=new JPanel();
        for(int i=0;i<player.getMurals().size();i++){
            icon=new JLabel(new ImageIcon(player.getMurals().get(i).getSourcePath()));
            panel.add(icon);
        }
        this.add(panel);
    }

}
