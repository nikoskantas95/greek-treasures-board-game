/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import javax.swing.JFrame;

/**
 *
 * @author John
 */
public class StatueWindow extends JFrame {

    private JPanel panel;

    private Statue statue;
    private ImageIcon icon;
    private Image img, newimg;
    private JLabel photo, info;

    public StatueWindow(Statue statue) {
        this.setSize(1000, 300);
        this.setTitle(statue.getMessage());
        this.setVisible(true);
        this.panel = new JPanel();
        panel.setLayout(new GridLayout(2, 1));
        this.photo = new JLabel(new ImageIcon(statue.getSourcePath()));      
        this.icon = new ImageIcon(statue.getSourcePath());
        img = icon.getImage();
        newimg = img.getScaledInstance(150, 100, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        this.photo.setIcon(icon);
        info = new JLabel();
        info.setText(statue.getDescription());
        info.setVisible(true);
        panel.add(photo);
        panel.add(info);
        this.add(panel);
    }

}
