/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.*;
import Model.*;
import Model.Player;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.URL;
import java.awt.event.*;
import java.util.*;

/**
 *
 * @author John
 */
public class WinnerWindow extends JFrame {

    private JPanel pan;
    private JLabel lab, win, stats;

    public WinnerWindow(Player winner) {
        this.setSize(new Dimension(800, 800));
        this.setTitle("You won!!!");
        this.pan = new JPanel();
        pan.setLayout(new GridLayout(0, 2));
        this.lab = new JLabel(new ImageIcon("resources/images/win.gif"));
        if (winner != null) {
            this.win = new JLabel(winner.getName() + " WINS!");
        } else {
            this.win = new JLabel("ITS A DRAW!");
        }

        win.setFont(new Font("Arial Black", Font.PLAIN, 36));
        pan.add(lab);
        pan.add(win);
        this.add(pan);
        this.setVisible(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

}
