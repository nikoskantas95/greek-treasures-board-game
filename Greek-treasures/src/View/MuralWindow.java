/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author John
 */
public class MuralWindow extends JFrame {

    private JButton but;
    private JPanel panel;
    private Player player;
    private Murals mural;
    private ImageIcon icon;
    private Image img, newimg;
    private JLabel photo, info;
    // private Murals murals[];
    

    public MuralWindow(Player player, Murals mural) {
        this.setSize(1000, 300);
        this.setTitle(mural.getMessage());
        this.setVisible(true);
        this.player = player;
        this.mural = mural;
        this.panel = new JPanel();
        panel.setLayout(new GridLayout(2, 1));
        this.photo = new JLabel(new ImageIcon(mural.getSourcePath()));      
        this.icon = new ImageIcon(mural.getSourcePath());
        img = icon.getImage();
        newimg = img.getScaledInstance(250, 100, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        this.photo.setIcon(icon);
        info=new JLabel(mural.getDescription());
        info.setVisible(true);       
        panel.add(photo);
        panel.add(info);      
        this.add(panel);

    }

}
